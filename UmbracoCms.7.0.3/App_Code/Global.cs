﻿using BeerRecipes.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

/// <summary>
/// Summary description for Global
/// </summary>
public class Global : Umbraco.Web.UmbracoApplication
{
    protected override void OnApplicationStarted(object sender, EventArgs e)
    {
        AreaRegistration.RegisterAllAreas();

        Config.Register(GlobalConfiguration.Configuration);

        GlobalConfiguration.Configuration.EnsureInitialized();
    }
}