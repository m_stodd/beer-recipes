﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BeerRecipes.Web.Api.Controllers
{
    [RoutePrefix("api/values")]
    public class ValuesController : ApiController
    {
        [Route("")]
        public List<string> GetValues()
        {
            return new List<string>()
            {
                "Value One",
                "Value two"
            };
        }
    }
}
